import {LitElement, html} from 'lit-element'

class FichaPersona extends LitElement {

    static get properties() {
        
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super();
        this.name = "Prueba Nombre";
        this.yearsInCompany="6";
        this.updatePersonInfo();
    }
    // Esto es la plantilla, para que esto cambie hay que cambiar
    // el valor de las propiedades
    render() {
        return html`
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
                <br />
            </div>
        `;
    }

    updated (changedProperties) {
        //changedProperties.forEach((oldValue, propName) => {
            
        //});
        if (changedProperties.has("name")) {
            console.log("Propiedad "+ propName +" cambia valor, anterior era " + oldValue)
        }

        if (changedProperties.has("yearsInCompany")) {
            this.updatePersonInfo();
        }

    }

    updateName(e) {
        console.log("updateName");
        this.name = e.target.value;

    }

    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
        // this.updatePersonInfo(); esto serí incorrecto
    }

    updatePersonInfo() {
        console.log("updatePersonInfo");
        console.log("yearsInCompany vale "+ this.yearsInCompany);
        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        };
    }
}

customElements.define('ficha-persona', FichaPersona)