import {LitElement, html} from 'lit-element'

class PersonaFichaListado extends LitElement {
    
    static get properties() {
        return {
            fname: {type: String},
            yearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="card">
                <img src="${this.photo.src}" height="50" width="50" alt="${this.photo.alt}">
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <div class="card-text">
                        

                            <h7>${this.profile}</h7> <br />
                            <h6>${this.yearsInCompany} años en la empresa</h6>
                        
                    </div>
                </div>               
                <div class="card-footer">
                    <button class="btn btn-danger col-5" @click="${this.deletePerson}"><strong>X</strong></button>
                    <button class="btn btn-info col-5 offset-1" @click="${this.moreInfo}"><strong>Info</strong></button>
                </div>
            </div>
        `;
    }
    deletePerson(e) {
        console.log("Se va a  borrar la persona de nombre " + this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person",{
                "detail" : {
                    "name" : this.fname 
                }

            })
        );
    }
    moreInfo(e) {
        console.log("moreInfo: se ha pedido más infor de la persona " + this.fname);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                "detail" : {
                    "name" : this.fname
                }
            })
        );
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado)