import {LitElement, html, css} from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {
    


    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name:"Carlos Gutiérrez",
                yearsInCompany: 10,
                profile: "Lorem ipsum risus iaculis placerat convallis dictum volutpat vitae, lacinia lacus suspendisse sollicitudin nullam massa pellentesque primis nisl, tincidunt dictum feugiat porta turpis praesent bibendum. justo sagittis nibh id, felis.",
                photo: {
                src: "./img/persona1.jpg",
                alt: "Carlos Gutiérrez"
                }
            }, {
                name:"Laura Pinto",
                yearsInCompany: 2,
                profile: "Lorem ipsum risus iaculis placerat convallis dictum volutpat vitae, lacinia lacus suspendisse sollicitudin nullam massa pellentesque primis nisl, tincidunt dictum feugiat porta turpis praesent bibendum. justo sagittis nibh id, felis.",
                photo: {
                    src: "./img/persona2.jpg",
                    alt: "Laura Pinto"
                    }
            }, {
                name:"Marcos Gómez",
                yearsInCompany: 5,
                profile: "Lorem ipsum risus iaculis placerat convallis dictum volutpat vitae, lacinia lacus suspendisse sollicitudin nullam massa pellentesque primis nisl, tincidunt dictum feugiat porta turpis praesent bibendum. justo sagittis nibh id, felis.",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Marcos Gómez"
                    }
            },
            {
                name:"Julio García",
                yearsInCompany: 5,
                profile: "de dónde sale esto",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Juio García"
                    }
            },
            {
                name:"Eduardo Zamarra",
                yearsInCompany: 5,
                profile: "Lorem ipsum risus iaculis placerat convallis dictum volutpat vitae, lacinia lacus suspendisse sollicitudin nullam massa pellentesque primis nisl, tincidunt dictum feugiat porta turpis praesent bibendum. justo sagittis nibh id, felis.",
                photo: {
                    src: "./img/persona3.jpg",
                    alt: "Eduardo Zamarra"
                    }
            }
        ];
        this.showPersonForm = false;
    }

    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4 card-group">
                ${this.people.map(
                    person => html`<persona-ficha-listado
                            fname="${person.name}"
                            profile="${person.profile}"
                            yearsInCompany="${person.yearsInCompany}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                                                        
                        >
                        </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                    <persona-form
                        id="personForm"
                        rounded border-primary
                        class="d-none"
                        @person-form-close="${this.personFormClose}"
                        @person-form-store="${this.personFormStore}"></persona-form>
            </div>
        `;
    }

    updated (changedProperties) {
        console.log("updated");
        console.log(changedProperties);

        if (changedProperties.has("showPersonForm")) {
            console.log ("Ha cambiado el valor de la propiedad showPersonForm en Persona-main");
            
            // true == "true" solo compara valor, === compara tipo y valor

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona main");

            this.dispatchEvent (new CustomEvent("updated-people",{
                "detail" : {
                    people: this.people
                }
            }))
        }
    }

    showPersonFormData (){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }


    showPersonList (){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");

        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    personFormClose () {
        console.log("personFormClose: se ha cerrado el formulario de añadir persona");

        this.showPersonForm = false;
    }

    personFormStore (e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log( "Name: " + e.detail.person.name);
        console.log( "Profile " + e.detail.person.profile);
        console.log( "Years in Company " + e.detail.person.yearsInCompany);
        console.log("La propiedad editingPerson vale "+ e.detail.editingPerson)
        
        if(e.detail.editingPerson === true) {

            console.log("Se va a aactualziar la persona de nomnre " + e.detail.person.name)
            //let indexOfPerson = this.people.findIndex(
            //    person => person.name === e.detail.person.name
            // );
            //if (indexOfPerson >= 0) {
            //    console.log("Persona encontrada");
            //    this.people[indexOfPerson] = e.detail.person;
            //}

            this.people = this.people.map(
                person => person.name === e.detail.name ? person = e.detail.person : person
            )

        } else {
            // this.people.push(e.detail.person);
            this.people = [ ...this.people,e.detail.person];
        }
        console.log ("Persona almacenada");
        this.showPersonForm = false;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("se va a borrar " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name  
        );
    }

    infoPerson(e) {
        console.log("infoPerson: se ha pedido más información de la persona " + e.detail.name);

        let chosenPerson = this.people.filter (
            person => person.name === e.detail.name
        );

        console.log (chosenPerson);
        
        let personToSHow = {};
        personToSHow.name = chosenPerson[0].name
        personToSHow.profile= chosenPerson[0].profile
        personToSHow.yearsInCompany= chosenPerson[0].yearsInCompany

        this.shadowRoot.getElementById("personForm").person = personToSHow
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }
}

customElements.define('persona-main', PersonaMain)