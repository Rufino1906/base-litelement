import {LitElement, html} from 'lit-element'

class PersonaStats extends LitElement {
    

//    render() {
//        return html`
//           <h1>Prueba persona stats</h1>
//        `;
//   }
    
    static get properties() {
        return {
            people: {type:Array}
        };
    }

    constructor() {
        super();

        this.people =[];

    }

    updated(changedProperties) {
        console.log("updated en persona-stats");

        if(changedProperties.has("people")) {
            console.log ("Ha cambiado el valor de la propiedad people en peson stats");

            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail: {
                    peopleStats: peopleStats
                }
            }))
        }
    }

    gatherPeopleArrayInfo(people) {
        console.log("gatherPeopleArrayInfo");

        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;

        return peopleStats;
    }

}

customElements.define('persona-stats', PersonaStats)